/*
 * =====================================================================================
 *
 *       Filename:  hooks.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:57:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */
#include "hooks.h"

void init_memhook_library(void)
{
		fprintf(stderr, "%s\n", __FUNCTION__ );

		libc_malloc = dlsym(RTLD_NEXT, "malloc");

		if ( libc_malloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read malloc symbol\n");
			abort();
		}

		libc_free   = dlsym(RTLD_NEXT, "free");

		if ( libc_malloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read free symbol\n");
			abort();
		}

		libc_realloc = dlsym(RTLD_NEXT, "realloc");

		if ( libc_realloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read realloc symbol\n");
			abort();
		}

		// symbols_intercepted = 1;
		switch_on_hooks();
};

void finalize_memhook_library(void)
{
	fprintf(stderr, "Number of calls to malloc   %d\n", malloc_count  );
	fprintf(stderr, "Number of calls to realloc  %d\n", realloc_count );
	fprintf(stderr, "Number of calls to free     %d\n", free_count    );
};

void switch_off_hooks(void)
{
	malloc_hook_active  = 0;
	realloc_hook_active = 0;
	free_hook_active    = 0;
};

void switch_on_hooks(void)
{
	malloc_hook_active  = 1;
	realloc_hook_active = 1;
	free_hook_active    = 1;
};

void* malloc (size_t size)
{
	// fprintf(stderr, "Calling malloc\n");
	// if ( !malloc_intercepted ) init_malloc();

  void *caller = __builtin_return_address(0);

  if (malloc_hook_active)
    return my_malloc_hook(size, caller);
	else
		return libc_malloc(size);
};


void*
my_malloc_hook (size_t size, void *caller)
{
  void *result;

  // deactivate hooks for logging
	switch_off_hooks();

  result = malloc(size);

  // do logging
	malloc_count++;
	fprintf(stderr, "  -> hooked to our malloc call!\n");

  // reactivate hooks
  malloc_hook_active = 1;
	switch_on_hooks();

  return (result);
};




void* realloc (void* ptr, size_t size)
{
	// fprintf(stderr, "Calling malloc\n");
	// if ( !malloc_intercepted ) init_malloc();

  void *caller = __builtin_return_address(0);

  if (realloc_hook_active)
    return my_realloc_hook(ptr, size, caller);
 	else 
		return libc_realloc(ptr,size);
};


void*
my_realloc_hook (void* ptr, size_t size, void *caller)
{
  // deactivate hooks for logging
	switch_off_hooks();

  ptr = realloc(ptr, size);

  // do logging
	realloc_count++;
	fprintf(stderr, "  -> hooked to our realloc call!\n");

  // reactivate hooks
  realloc_hook_active = 1;
	switch_on_hooks();

  return (ptr);
};

void free (void* ptr)
{
	// fprintf(stderr, "Calling malloc\n");
	// if ( !malloc_intercepted ) init_malloc();

  void *caller = __builtin_return_address(0);

  if (free_hook_active)
    my_free_hook(ptr, caller);
	else
		libc_free(ptr);
};

void
my_free_hook (void* ptr, void *caller)
{
  // deactivate hooks for logging
	switch_off_hooks();

  free(ptr);

  // do logging
	free_count++;
	fprintf(stderr, "  -> hooked to our free call!\n");

  // reactivate hooks
	switch_on_hooks();
};






/*
void init(void)
{
	if ( library_is_initialized == 0 ) {
		fprintf(stderr, "Initializing hooks!\n");
	
		libc_malloc = dlsym(RTLD_NEXT, "malloc");

		if ( libc_malloc == NULL ) {
			fprintf(stderr, "ERROR: cant read malloc symbol\n");
			abort();
		}
	
		fprintf(stderr, "Address of libc malloc %p\n", libc_malloc );
	}
};

void finalize(void)
{

};

void *malloc(size_t size)
{
	if ( libc_malloc == NULL ) {
		init();
	}

	fprintf(stderr, "malloc call!\n");
	
	void *p = libc_malloc(size);


	return (p);
};
*/
