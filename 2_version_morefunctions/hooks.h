/*
 * =====================================================================================
 *
 *       Filename:  hooks.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:54:51
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef _HOOKS_H_
#define _HOOKS_H_

#include <stdlib.h>
#include <stdio.h>

#define __GNU_SOURCE
#define _GNU_SOURCE
#define __USE_GNU
#include <dlfcn.h>

void *(*libc_malloc)  (size_t size)            = NULL;
void *(*libc_realloc) (void* ptr, size_t size) = NULL;
void  (*libc_free)    (void* ptr  )            = NULL;

// static int symbols_intercepted __attribute__((unused)) = 0;

static int malloc_hook_active  __attribute__((unused)) = 0;
static int realloc_hook_active __attribute__((unused)) = 0;
static int free_hook_active    __attribute__((unused)) = 0;

static int malloc_count       __attribute__((unused)) = 0;
static int realloc_count      __attribute__((unused)) = 0;
static int free_count         __attribute__((unused)) = 0;

__attribute__((constructor)) void init_memhook_library(void);
__attribute__((destructor))  void finalize_memhook_library(void);


void switch_off_hooks(void);
void switch_on_hooks(void);

void* malloc(size_t size);
void* my_malloc_hook (size_t size, void *caller);

void* realloc(void* ptr, size_t size);
void* my_realloc_hook (void* ptr, size_t size, void *caller);

void  free(void *ptr);
void  my_free_hook (void *ptr, void *caller);


/*
in library_is_initialined = 0;

static size_t malloc_calls_count = 0;
static size_t free_calls_count   = 0;
static size_t realloc_call_count = 0;

static void *(*libc_malloc) (size_t) = NULL;


void init(void);
void finalize(void);

void *malloc(size_t size);
*/

#endif
