/*
 * =====================================================================================
 *
 *       Filename:  mylib.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:46:35
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */
#pragma once

#include <stdlib.h>
#include <stdio.h>

void mylib_function (void);

