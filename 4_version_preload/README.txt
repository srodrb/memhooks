This code implements a simple hooking procedure for malloc().

It works for static libraries, but the hooks must be compiled dynamically
LD_DEBUG=all  ./main_so 2>log_dyn
