/*
 * =====================================================================================
 *
 *       Filename:  hooks.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:57:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */
#include "hooks.h"

void init_memhook_library(void)
{
		fprintf(stderr, "\n\n\n      LIBHOOK UTILITY INFO:\n\n\n");

		fprintf(stderr, "\t\t%s at %lf\n", __FUNCTION__, binary_start_timestamp );
	
		libc_calloc = temporary_calloc;
		libc_calloc = dlsym(RTLD_NEXT, "calloc");

		if ( libc_calloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read calloc symbol\n");
			abort();
		}
		fprintf(stderr, "\t\t--- calloc hooked\n");

	
		libc_malloc = dlsym(RTLD_NEXT, "malloc");

		if ( libc_malloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read malloc symbol\n");
			abort();
		}
		fprintf(stderr, "\t\t--- malloc hooked\n");

		libc_valloc = dlsym(RTLD_NEXT, "valloc");

		if ( libc_valloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read valloc symbol\n");
			abort();
		}
		fprintf(stderr, "\t\t--- valloc hooked\n");


		libc_posix_memalign = dlsym(RTLD_NEXT, "posix_memalign");

		if ( libc_posix_memalign == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read posix_memalign symbol\n");
			abort();
		}
		fprintf(stderr, "\t\t--- posix_memalign hooked\n");


		libc_free   = dlsym(RTLD_NEXT, "free");

		if ( libc_free == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read free symbol\n");
			abort();
		}
		fprintf(stderr, "\t\t--- free hooked\n");

		libc_realloc = dlsym(RTLD_NEXT, "realloc");

		if ( libc_realloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read realloc symbol\n");
			abort();
		}
		fprintf(stderr, "\t\t--- realloc hooked\n");

#ifdef __INTEL_COMPILER
		intel_mm_malloc = dlsym(RTLD_NEXT, "_mm_malloc");

		if ( intel_mm_malloc == NULL )
		{
			fprintf(stderr, "WARNING: cant read _mm_malloc symbol\n");
		}
		fprintf(stderr, "\t\t--- _mm_malloc hooked\n");

		intel_mm_free = dlsym(RTLD_NEXT, "_mm_free");
		if ( intel_mm_free == NULL )
		{
			fprintf(stderr, "WARNING: cant read _mm_free symbol\n");
		}
		fprintf(stderr, "\t\t--- _mm_free hooked\n");
#endif

		fprintf(stderr, "\n\n\n");

		/*
		static void (*mkl_free) (void*) __attribute__((unused)) = NULL;
		mkl_free = dlsym(RTLD_NEXT, "mkl_free");
		if ( mkl_free == NULL )
		{
			fprintf(stderr, "WARNING: cant read mkl_free symbol\n");
		}
		fprintf(stderr, "--- mkl_free hooked\n");
		*/



		memlogfile = fopen("memlog.csv", "w");

		if ( memlogfile == NULL )
		{
			fprintf(stderr, "ERROR: cant open log file correctly\n");
			abort();
		}

	/* enable instrumentation */
	switch_on_instrumentation();

	/* switch on hooks flags */
	switch_on_hooks();
	
	/* Get a timestamp, once the shared library is initialized */	
	binary_start_timestamp = memhook_get_timestamp();
};

void finalize_memhook_library(void)
{
	fprintf(stderr, "\n\t\tApplication took      %.4fs   \n\n", memhook_get_timestamp() );
	
	fprintf(stderr, "\t\tNumber of calls to malloc            %d\n", malloc_count         );
	fprintf(stderr, "\t\tNumber of calls to valloc            %d\n", valloc_count         );
	fprintf(stderr, "\t\tNumber of calls to calloc            %d\n", calloc_count         );
	fprintf(stderr, "\t\tNumber of calls to realloc           %d\n", realloc_count        );
	fprintf(stderr, "\t\tNumber of calls to free              %d\n", free_count           );
	fprintf(stderr, "\t\tNumber of calls to posix_memalign    %d\n", posix_memalign_count );

#ifdef __INTEL_COMPILER
	fprintf(stderr, "\t\tNumber of calls to _mm_malloc        %d\n", _mm_malloc_count     );
	fprintf(stderr, "\t\tNumber of calls to _mm_free          %d\n", _mm_free_count       );
#endif

	int allocs = malloc_count + valloc_count + calloc_count + posix_memalign_count + _mm_malloc_count;
	int frees  = free_count + _mm_free_count;

	if ( allocs != frees )
		fprintf(stderr, "\n\t\tWARNING: %d leaks detected!\n", allocs - frees );
	else
		fprintf(stderr, "\n\t\tINFO:no leaks detected! %d allocs %d frees\n\n", allocs, frees );


	/* 
	 * Restore the hooks to the free() can be used to unload the shared
	 * library and fclose correctly.
	 */	
	switch_off_hooks();

	 /*
		* Close logging file
		*/
	if ( memlogfile ) { fclose(memlogfile); };
};

void switch_off_instrumentation(void)
{
	is_instrumentation_active = 0;
};


void switch_on_instrumentation(void)
{
	is_instrumentation_active = 1;
};

void switch_off_hooks(void)
{
	malloc_hook_active         = 0;
	valloc_hook_active         = 0;
	calloc_hook_active         = 0;
	realloc_hook_active        = 0;
	free_hook_active           = 0;
	posix_memalign_hook_active = 0;
#ifdef __INTEL_COMPILER
	_mm_malloc_hook_active     = 0;
	_mm_free_hook_active       = 0;
#endif
};

void switch_on_hooks(void)
{
	malloc_hook_active         = 1;
	valloc_hook_active         = 1;
	calloc_hook_active         = 1;
	realloc_hook_active        = 1;
	free_hook_active           = 1;
	posix_memalign_hook_active = 1;
#ifdef __INTEL_COMPILER
	_mm_malloc_hook_active     = 1;
	_mm_free_hook_active       = 1;
#endif
};

void* malloc (size_t size)
{
  //void *caller = __builtin_return_address(0);

	// fprintf(stderr, "---------- caller: %p\n", caller);

  if (malloc_hook_active && is_instrumentation_active )
    return my_malloc_hook(size);
	else
		return libc_malloc(size);
};


void*
my_malloc_hook (size_t size)
{
  void *result;

  // deactivate hooks for logging
	switch_off_hooks();

  /* do logging */
	trace_symbols();

  result = malloc(size);
	log_event( MALLOC_FUNCTION, result, NULL, size );

	malloc_count++;

  /* reactivate hooks */
	switch_on_hooks();

  return (result);
};

void* valloc (size_t size)
{
  //void *caller = __builtin_return_address(0);

  if (valloc_hook_active && is_instrumentation_active )
    return my_valloc_hook(size);
	else
		return libc_valloc(size);
};


void*
my_valloc_hook (size_t size)
{
  void *result;

  // deactivate hooks for logging
	switch_off_hooks();

  /* do logging */
	trace_symbols();

  result = valloc(size);
	log_event( MALLOC_FUNCTION, result, NULL, size );

	valloc_count++;

  /* reactivate hooks */
	switch_on_hooks();

  return (result);
};

void* temporary_calloc(size_t nmemb, size_t size)
{
	return NULL;
};

void* calloc (size_t nmemb, size_t size)
{
  //void *caller = __builtin_return_address(0);

  if (calloc_hook_active && is_instrumentation_active )
    return my_calloc_hook(nmemb, size);
	else
		return libc_calloc(nmemb, size);
};


void*
my_calloc_hook (size_t nmemb, size_t size)
{

  // deactivate hooks for logging
	switch_off_hooks();
  
	void *result;

  /* do logging */
	trace_symbols();

  result = calloc(nmemb, size);
	log_event( MALLOC_FUNCTION, result, NULL, size );

	calloc_count++;

  /* reactivate hooks */
	switch_on_hooks();

  return (result);
};


void* realloc (void* ptr, size_t size)
{
  //void *caller = __builtin_return_address(0);

  if (realloc_hook_active && is_instrumentation_active )
    return my_realloc_hook(ptr, size);
 	else 
		return libc_realloc(ptr,size);
};

void*
my_realloc_hook (void* ptr, size_t size)
{
	void *oldptr = ptr;

	switch_off_hooks();

	/* do logging */
	trace_symbols();
 	
	/* There are some edge cases to handle here */
	if ( ptr == NULL ) {
		ptr = realloc(ptr, size);
		log_event( MALLOC_FUNCTION, ptr, NULL, size );
		malloc_count++;
	}
	else {
		ptr = realloc(ptr, size);
		log_event( REALLOC_FUNCTION, oldptr, ptr, size );
		realloc_count++;
	}

  /* reactivate hooks */
	switch_on_hooks();

  return (ptr);
};

void free (void* ptr)
{
  //void *caller = __builtin_return_address(0);

  if (free_hook_active && is_instrumentation_active )
    my_free_hook(ptr);
	else
		libc_free(ptr);
};

void
my_free_hook (void* ptr)
{
  // deactivate hooks for logging
	switch_off_hooks();

  /* do logging */
	trace_symbols();
	log_event( FREE_FUNCTION, ptr, NULL, 0 );

  free(ptr);

	free_count++;

  /* reactivate hooks */
	switch_on_hooks();
};

int posix_memalign(void **memptr, size_t alignment, size_t size)
{
  //void *caller = __builtin_return_address(0);

  if (posix_memalign_hook_active && is_instrumentation_active )
    return (my_posix_memalign_hook(memptr, alignment, size));
	else
		return (libc_posix_memalign( memptr, alignment, size));
};


int my_posix_memalign_hook(void **memptr, size_t alignment, size_t size)
{

  // deactivate hooks for logging
	switch_off_hooks();

  /* do logging */
	trace_symbols();

  int result = posix_memalign(memptr, alignment, size);
	log_event( MALLOC_FUNCTION, &memptr, NULL, alignment + size );

	posix_memalign_count++;

  /* reactivate hooks */
	switch_on_hooks();

  return (result);
};

#ifdef __INTEL_COMPILER
	void* _mm_malloc (size_t size, size_t alignment)
	{
	  // void *caller = __builtin_return_address(0);
	
	  if (_mm_malloc_hook_active && is_instrumentation_active )
	    return my_mm_malloc_hook(size, alignment);
		else
			return intel_mm_malloc(size, alignment);
	};
	
	
	void*
	my_mm_malloc_hook (size_t size, size_t alignment)
	{
	  void *result;
	
	  // deactivate hooks for logging
		switch_off_hooks();
	
	  /* do logging */
		trace_symbols();
	
	  result = _mm_malloc(size, alignment );
		log_event( MALLOC_FUNCTION, result, NULL, size );
	
		_mm_malloc_count++;
	
	  /* reactivate hooks */
		switch_on_hooks();
	
	  return (result);
	};
	
	void _mm_free (void* ptr)
	{
	  // void *caller = __builtin_return_address(0);
	
	  if (_mm_free_hook_active && is_instrumentation_active )
	    my_mm_free_hook(ptr);
		else
			intel_mm_free(ptr);
	};
	
	void
	my_mm_free_hook (void* ptr)
	{
	  // deactivate hooks for logging
		switch_off_hooks();
	
	  /* do logging */
		trace_symbols();
		log_event( FREE_FUNCTION, ptr, NULL, 0 );
	
	  _mm_free(ptr);
	
		free_count++;
	
	  /* reactivate hooks */
		switch_on_hooks();
	};
#endif



double memhook_get_timestamp(void)
{
	double tseconds = (double) 0.0;
	struct timeval mytime;
	gettimeofday ( &mytime, (struct timezone*) 0);
	tseconds = (double) (mytime.tv_sec + mytime.tv_usec * 1.0e-6);
	return (tseconds - binary_start_timestamp );
};

#if defined (enable_backtracing)
	void trace_symbols ( void )
	{
		void *array[BACKTRACE_DEPTH];
		size_t nframes, frame; 
		char **strings;
	
		nframes = backtrace (array, BACKTRACE_DEPTH);
		
		if ( nframes ) {
			strings = backtrace_symbols( array, nframes );
			
			nframes = (BACKTRACE_DEPTH >= nframes) ? nframes: BACKTRACE_DEPTH;
	
			for(frame = 2; frame < nframes; frame++) {
				fprintf(memlogfile, "trace %zd :: %s\n", frame, strings[frame] );

				// ParseBacktraceString( strings[frame] );
			}
		
			free(strings);
		}
		else {
			fprintf(stderr, "WARNING: backtrace() did not recover any frame\n");
		}
	};
	
	void ParseBacktraceString ( char* s )
	{
		size_t spaceCount=0, index=0, start=0, end=0;
		char *function_name;
		char *c = s;                /* current value */
		char *n = c + 1;            /* next value    */
	
		/*
		 * We're using to while statements to detect the start and the
		 * end indices of the sub-string we want to extract.
		 */
		while ( *n  && ( spaceCount != 3 ) ) {
			if ( (*c == ' ') && (*n != ' ') ) { spaceCount++; }
			c++; n++; index++;
		}
		start = index;
	
		while ( *n  && ( spaceCount != 4 ) ) {
			if ( (*c != ' ') && (*n == ' ') ) {  spaceCount++; }
			c++; n++; index++;
		}
		end = index;
	
		/* Allocate space and copy the sub-string.*/
		function_name = (char*) malloc ( (end - start + 1) * sizeof(char));
		function_name = strncpy ( function_name, &s[start], (end - start));
		function_name[end - start] = '\0';
	
		fprintf(stderr, "\t\t%s\n", function_name );
	
		free(function_name);
	
		// return (function_name);
	};
#endif

void log_event( function_t id, void *base, void *mod, size_t size )
{
	if ( mod != NULL ) {
		fprintf( memlogfile, "%d,%p,%p,%lu,%f,%s,%d\n", (int) id, base, mod, size, \
			memhook_get_timestamp(), "None", (int) 0 );
	}
	else {
		/* Some unix-like systems will print (nil) is the pointer is null */
		fprintf( memlogfile, "%d,%p,%s,%lu,%f,%s,%d\n", (int) id, base, "0x0", size, \
			memhook_get_timestamp(), "None", (int) 0 );
	}
};

