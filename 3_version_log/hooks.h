/*
 * =====================================================================================
 *
 *       Filename:  hooks.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:54:51
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef _HOOKS_H_
#define _HOOKS_H_

#include <stdlib.h>
#include <stdio.h>

#define __GNU_SOURCE
#define _GNU_SOURCE
#define __USE_GNU
#include <dlfcn.h>

void *(*libc_malloc)  (size_t size)            = NULL;
void *(*libc_realloc) (void* ptr, size_t size) = NULL;
void  (*libc_free)    (void* ptr  )            = NULL;

// static int symbols_intercepted __attribute__((unused)) = 0;

static int is_instrumentation_active __attribute__((unused)) = 0;

static int malloc_hook_active  __attribute__((unused)) = 0;
static int realloc_hook_active __attribute__((unused)) = 0;
static int free_hook_active    __attribute__((unused)) = 0;

static int malloc_count       __attribute__((unused)) = 0;
static int realloc_count      __attribute__((unused)) = 0;
static int free_count         __attribute__((unused)) = 0;

static double binary_start_timestamp __attribute__((unused)) = 0.0;

/*
 * Dynamic library initialization and finalization routines.
 * These will be executed when the library is loaded/unloaded
 * and do no require further user intervention.
 */
__attribute__((constructor)) void init_memhook_library(void);
__attribute__((destructor))  void finalize_memhook_library(void);


/*
 * Switches on/off all hook functions. This is critical for logging
 * purposes.
 */
void switch_off_hooks(void);
void switch_on_hooks(void);

/*
 * Enables/disables instrumentation on an specific code region.
 */
void switch_off_instrumentation(void);
void switch_on_instrumentation(void);

void* malloc(size_t size);
void* my_malloc_hook (size_t size, void *caller);

void* realloc(void* ptr, size_t size);
void* my_realloc_hook (void* ptr, size_t size, void *caller);

void  free(void *ptr);
void  my_free_hook (void *ptr, void *caller);

/* Define timers */
#include <time.h>
#include <sys/time.h>

double memhook_get_timestamp(void);




#if defined(enable_backtracing)
	#include <execinfo.h> /* needed for backtrace */
	#include <string.h>

	/*
	 * The first stack frame contains the name of the inspecting function
	 * and that will be discarted. Thus, stackframe is the number of back traces
	 * we would like to have plus 1.
	 */
	#define BACKTRACE_DEPTH 5

	void trace_symbols ( void );

	/*
	 * https://stackoverflow.com/questions/9464780/strncpy-introduces-funny-character
	 * https://stackoverflow.com/questions/37915815/expression-result-unused-when-incrementing-a-pointer
	 */
	void ParseBacktraceString ( char* s );
#else
	#define trace_symbols() {}
#endif

/*
 * LOGING SYSTEM
 */
static FILE *memlogfile __attribute__((unused)) = NULL;
typedef enum {FREE_FUNCTION, MALLOC_FUNCTION, REALLOC_FUNCTION} function_t;


void log_event( function_t id, void *base, void *mod, size_t size ); 




#endif
