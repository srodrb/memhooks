/*
 * =====================================================================================
 *
 *       Filename:  hooks.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:57:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */
#include "hooks.h"

void init_memhook_library(void)
{
		binary_start_timestamp = memhook_get_timestamp();

		fprintf(stderr, "%s at %lf\n", __FUNCTION__, binary_start_timestamp );

		libc_malloc = dlsym(RTLD_NEXT, "malloc");

		if ( libc_malloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read malloc symbol\n");
			abort();
		}

		libc_free   = dlsym(RTLD_NEXT, "free");

		if ( libc_malloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read free symbol\n");
			abort();
		}

		libc_realloc = dlsym(RTLD_NEXT, "realloc");

		if ( libc_realloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read realloc symbol\n");
			abort();
		}

		memlogfile = fopen("memlog.csv", "w");

		if ( memlogfile == NULL )
		{
			fprintf(stderr, "ERROR: cant open log file correctly\n");
			abort();
		}

	/* enable instrumentation */
	switch_on_instrumentation();
	switch_on_hooks();
};

void finalize_memhook_library(void)
{
	fprintf(stderr, "Number of calls to malloc   %d\n", malloc_count  );
	fprintf(stderr, "Number of calls to realloc  %d\n", realloc_count );
	fprintf(stderr, "Number of calls to free     %d\n", free_count    );
	fprintf(stderr, "Application took      %.4fs   \n", memhook_get_timestamp() );
	if ( memlogfile ) { fclose(memlogfile); };
};

void switch_off_instrumentation(void)
{
	is_instrumentation_active = 0;
};


void switch_on_instrumentation(void)
{
	is_instrumentation_active = 1;
};

void switch_off_hooks(void)
{
	malloc_hook_active  = 0;
	realloc_hook_active = 0;
	free_hook_active    = 0;
};

void switch_on_hooks(void)
{
	malloc_hook_active  = 1;
	realloc_hook_active = 1;
	free_hook_active    = 1;
};

void* malloc (size_t size)
{
  void *caller = __builtin_return_address(0);

  if (malloc_hook_active && is_instrumentation_active )
    return my_malloc_hook(size, caller);
	else
		return libc_malloc(size);
};


void*
my_malloc_hook (size_t size, void *caller)
{
  void *result;

  // deactivate hooks for logging
	switch_off_hooks();

  /* do logging */
	trace_symbols();

  result = malloc(size);
	log_event( MALLOC_FUNCTION, result, NULL, size );

	malloc_count++;

  /* reactivate hooks */
	switch_on_hooks();

  return (result);
};

void* realloc (void* ptr, size_t size)
{
  void *caller = __builtin_return_address(0);

  if (realloc_hook_active && is_instrumentation_active )
    return my_realloc_hook(ptr, size, caller);
 	else 
		return libc_realloc(ptr,size);
};


void*
my_realloc_hook (void* ptr, size_t size, void *caller)
{
	double *oldptr = ptr;

	switch_off_hooks();

	/* do logging */
	trace_symbols();
  
	ptr = realloc(ptr, size);
	log_event( REALLOC_FUNCTION, oldptr, ptr, size );

	realloc_count++;

  /* reactivate hooks */
	switch_on_hooks();

  return (ptr);
};

void free (void* ptr)
{
  void *caller = __builtin_return_address(0);

  if (free_hook_active && is_instrumentation_active )
    my_free_hook(ptr, caller);
	else
		libc_free(ptr);
};

void
my_free_hook (void* ptr, void *caller)
{
  // deactivate hooks for logging
	switch_off_hooks();

  /* do logging */
	trace_symbols();
	log_event( FREE_FUNCTION, ptr, NULL, 0 );

  free(ptr);

	free_count++;

  /* reactivate hooks */
	switch_on_hooks();
};

double memhook_get_timestamp(void)
{
	double tseconds = (double) 0.0;
	struct timeval mytime;
	gettimeofday ( &mytime, (struct timezone*) 0);
	tseconds = (double) (mytime.tv_sec + mytime.tv_usec * 1.0e-6);
	return (tseconds - binary_start_timestamp );
};

#if defined (enable_backtracing)
	void trace_symbols ( void )
	{
		void *array[BACKTRACE_DEPTH];
		size_t nframes, frame; 
		char **strings;
	
		nframes = backtrace (array, BACKTRACE_DEPTH);
		strings = backtrace_symbols( array, nframes );
	
		nframes = (BACKTRACE_DEPTH >= nframes) ? nframes: BACKTRACE_DEPTH;
	
		for(frame = 3; frame < nframes; frame++)
			ParseBacktraceString( strings[frame] );
	
		free(strings);
	};
	
	void ParseBacktraceString ( char* s )
	{
		size_t spaceCount=0, index=0, start=0, end=0;
		char *function_name;
		char *c = s;                /* current value */
		char *n = c + 1;            /* next value    */
	
		/*
		 * We're using to while statements to detect the start and the
		 * end indices of the sub-string we want to extract.
		 */
		while ( *n  && ( spaceCount != 3 ) ) {
			if ( (*c == ' ') && (*n != ' ') ) { spaceCount++; }
			c++; n++; index++;
		}
		start = index;
	
		while ( *n  && ( spaceCount != 4 ) ) {
			if ( (*c != ' ') && (*n == ' ') ) {  spaceCount++; }
			c++; n++; index++;
		}
		end = index;
	
		/* Allocate space and copy the sub-string.*/
		function_name = (char*) malloc ( (end - start + 1) * sizeof(char));
		function_name = strncpy ( function_name, &s[start], (end - start));
		function_name[end - start] = '\0';
	
		fprintf(stderr, "\t\t%s\n", function_name );
	
		free(function_name);
	
		// return (function_name);
	};
#endif

void log_event( function_t id, void *base, void *mod, size_t size )
{
	fprintf( memlogfile, "%d,%p,%p,%lu,%f,%s,%d\n", \
			(int) id,
			base,
			mod,
			size,
			memhook_get_timestamp(),
		 "None",
	   (int) 0 );
};

