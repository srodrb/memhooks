/*
 * =====================================================================================
 *
 *       Filename:  hooks.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:57:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */
#include "hooks.h"

void init_malloc(void)
{
		fprintf(stderr, "%s\n", __FUNCTION__ );

		libc_malloc = dlsym(RTLD_NEXT, "malloc");

		if ( libc_malloc == NULL ) 
		{
			fprintf(stderr, "ERROR: cant read malloc symbol\n");
			abort();
		}

		malloc_intercepted = 1;
		malloc_hook_active = 1;
};

void* malloc (size_t size)
{
	fprintf(stderr, "Calling malloc\n");
	if ( !malloc_intercepted ) init_malloc();

  void *caller = __builtin_return_address(0);

  if (malloc_hook_active)
    return my_malloc_hook(size, caller);
  
	return libc_malloc(size);
};


void*
my_malloc_hook (size_t size, void *caller)
{
  void *result;

  // deactivate hooks for logging
  malloc_hook_active = 0;

  result = malloc(size);

  // do logging
	fprintf(stderr, "  -> hooked to our malloc call!\n");

  // reactivate hooks
  malloc_hook_active = 1;

  return result;
};


/*
void init(void)
{
	if ( library_is_initialized == 0 ) {
		fprintf(stderr, "Initializing hooks!\n");
	
		libc_malloc = dlsym(RTLD_NEXT, "malloc");

		if ( libc_malloc == NULL ) {
			fprintf(stderr, "ERROR: cant read malloc symbol\n");
			abort();
		}
	
		fprintf(stderr, "Address of libc malloc %p\n", libc_malloc );
	}
};

void finalize(void)
{

};

void *malloc(size_t size)
{
	if ( libc_malloc == NULL ) {
		init();
	}

	fprintf(stderr, "malloc call!\n");
	
	void *p = libc_malloc(size);


	return (p);
};
*/
