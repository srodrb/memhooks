/*
 * =====================================================================================
 *
 *       Filename:  mylib.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:47:13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */
#include "mylib.h"

void mylib_function(void)
{
	double *a, *b, *c;

	a = malloc(1);
	b = malloc(2);
	free(a);
	c = malloc(3);
	free(c);
	free(b);

	fprintf(stderr, "End of %s\n", __FUNCTION__ );
};
