/*
 * =====================================================================================
 *
 *       Filename:  hooks.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/05/2017 08:54:51
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu, (samuel.rodriguez@bsc.es)
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

/*
 * TODO: calloc function log as it were a malloc call(). Need to extend enum and
 * modify python script.
 *
 * Same applies to posix_memalign and valloc
 *
 * stderr as a variable! (weak symbol)
 */


#ifndef _HOOKS_H_
#define _HOOKS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Define timers */
#include <time.h>
#include <sys/time.h>
#include <unistd.h>


double memhook_get_timestamp(void);

#if defined (enable_backtracing)
		#include <execinfo.h> /* needed for backtrace */
	
		/*
		 * The first stack frame contains the name of the inspecting function
		 * and that will be discarted. Thus, stackframe is the number of back traces
		 * we would like to have plus 1.
		 */
		#define BACKTRACE_DEPTH 5
	
		/*
		 * It reads the stack using backtrace call. Then, it uses
		 * ParseBacktraceString to demangle the symbols.
		 */
		void trace_symbols ( void );
	
		/*
		 * https://stackoverflow.com/questions/9464780/strncpy-introduces-funny-character
		 * https://stackoverflow.com/questions/37915815/expression-result-unused-when-incrementing-a-pointer
		 */
		void ParseBacktraceString ( char* s );
#else
	#define trace_symbols() {}
#endif


#define __GNU_SOURCE
#define _GNU_SOURCE
#define __USE_GNU
#include <dlfcn.h>

void *(*libc_malloc)         (size_t size)               = NULL;
void *(*libc_valloc)         (size_t size)               = NULL;
void *(*libc_calloc)         (size_t nmemb, size_t size) = NULL;
void *(*libc_realloc)        (void* ptr, size_t size)    = NULL;
void  (*libc_free)           (void* ptr  )               = NULL;
int   (*libc_posix_memalign) (void **, size_t, size_t)   = NULL;

#ifdef __INTEL_COMPILER
	void *(*intel_mm_malloc) (size_t size, size_t align ) = NULL;
	void  (*intel_mm_free  ) (void *ptr)                  = NULL;
#endif


static int   is_instrumentation_active  __attribute__((unused)) = 0;

static int malloc_hook_active         __attribute__((unused)) = 0;
static int valloc_hook_active         __attribute__((unused)) = 0;
static int calloc_hook_active         __attribute__((unused)) = 0;
static int realloc_hook_active        __attribute__((unused)) = 0;
static int free_hook_active           __attribute__((unused)) = 0;
static int posix_memalign_hook_active __attribute__((unused)) = 0;

#ifdef __INTEL_COMPILER
	static int _mm_malloc_hook_active     __attribute__((unused)) = 0;
	static int _mm_free_hook_active       __attribute__((unused)) = 0;
#endif

static int malloc_count          __attribute__((unused)) = 0;
static int valloc_count          __attribute__((unused)) = 0;
static int calloc_count          __attribute__((unused)) = 0;
static int realloc_count         __attribute__((unused)) = 0;
static int free_count            __attribute__((unused)) = 0;
static int posix_memalign_count  __attribute__((unused)) = 0;
static int _mm_malloc_count      __attribute__((unused)) = 0;
static int _mm_free_count        __attribute__((unused)) = 0;

/*
 * This variable contains the startup time of the binary.
 */
static double binary_start_timestamp __attribute__((unused)) = 0.0;

/*
 * Information about MPI rank size and local rank id.
 */
static int rank_size __attribute__((unused)) = 1;
static int rank      __attribute__((unused)) = 0;

/*
 * Dynamic library initialization and finalization routines.
 * These will be executed when the library is loaded/unloaded
 * and do no require further user intervention.
 */
__attribute__((constructor)) void init_memhook_library(void);
__attribute__((destructor))  void finalize_memhook_library(void);


/*
 * Switches on/off all hook functions. This is critical for logging
 * purposes.
 */
void switch_off_hooks(void);
void switch_on_hooks(void);

/*
 * Enables/disables instrumentation on an specific code region.
 */
void switch_off_instrumentation(void);
void switch_on_instrumentation(void);

void* malloc(size_t size);
void* my_malloc_hook (size_t size);

void* valloc(size_t size);
void* my_valloc_hook (size_t size);

/*
 * In order to hook calloc by dlsym we should create a
 * fake calloc function.
 *
 * http://blog.bigpixel.ro/2010/09/interposing-calloc-on-linux/
 */
void* temporary_calloc(size_t nmemb, size_t size);
void* calloc(size_t nmemb, size_t size);
void* my_calloc_hook (size_t nmemb, size_t size);

void* realloc(void* ptr, size_t size);
void* my_realloc_hook (void* ptr, size_t size);

void  free(void *ptr);
void  my_free_hook (void *ptr);

int posix_memalign(void **memptr, size_t alignment, size_t size);
int my_posix_memalign_hook(void **memptr, size_t alignment, size_t size);


#ifdef __INTEL_COMPILER
	void* _mm_malloc(size_t size, size_t alignment);
	void* my_mm_malloc_hook(size_t size, size_t alignment);

	void  _mm_free(void *ptr);
	void  my_mm_free_hook(void *ptr);
#endif

/*
 * LOGING SYSTEM
 */
static FILE *memlogfile __attribute__((unused)) = NULL;
typedef enum {FREE_FUNCTION, MALLOC_FUNCTION, REALLOC_FUNCTION} function_t;


void log_event( function_t id, void *base, void *mod, size_t size ); 




#endif
