/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  28/05/2017 10:43:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu (), samuel.rodriguez@bsc.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

// #include "hooks.h"
#include <mpi.h>
#include "mylib.h"



int main(int argc, const char *argv[])
{
	/* Initialize MPI environment */
	MPI_Init(NULL, NULL);

	int rank, ranksize;

	MPI_Comm_size( MPI_COMM_WORLD, &ranksize);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );


	if ( rank == 0 ) { 
		fprintf(stderr, "Inicio del programa\n");
		
		double *a, *b, *c;

		a = malloc(20);
		b = malloc(5);
		free(a);
		c = malloc(15);
		free(c);
		free(b);
		
		fprintf(stderr, "Ahora llamamos a la libreria\n");
	}
	
	mylib_function();

	MPI_Barrier( MPI_COMM_WORLD );

	if ( rank == 0 ) {
		fprintf(stderr, "Fin del programa\n");
	}

	MPI_Finalize();
	return 0;
}
